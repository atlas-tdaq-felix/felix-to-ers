#include "felix2atlas/SubscribeClient.hpp"
    
void daq::felix2atlas::SubscribeClient::on_init() {
    printf("on_init called\n");
}

void daq::felix2atlas::SubscribeClient::on_connect(uint64_t fid) {
    printf("on_connect called 0x%lx\n", fid);
}

void daq::felix2atlas::SubscribeClient::on_disconnect(uint64_t fid) {
    printf("on_disconnect called 0x%lx\n", fid);
}

void daq::felix2atlas::SubscribeClient::on_data(std::string fifo, const uint8_t* data, size_t size, uint8_t status) {

    if (sinkByFifo.find(fifo)!= sinkByFifo.end()){
        auto sink = (sinkByFifo.find(fifo)->second);
        char* lineptr = strtok((char*)data, "\n");
        while (lineptr != NULL){
            //std::cout << "Message received: "<< std::string(data, data + size) << std::endl;   
            try {
                sink->dispatch(std::string(lineptr));
            } catch (std::exception &ex) {
                ::ers::error(felix2atlas::JsonIssue(ERS_HERE, std::string("Can't dispatch message: ") + ex.what()));
            }
            lineptr = strtok(NULL, "\n");
        }
    } else {
        ::ers::error(felix2atlas::Felix2atlasIssue(ERS_HERE, "sink not found"));
    }
}

std::string daq::felix2atlas::SubscribeClient::extractField(std::string field,const std::string &message){
    nlohmann::json root;
    std::stringstream(message) >> root;
    std::string val = root[field].get<std::string>();
    return val;   
}

void daq::felix2atlas::SubscribeClientFIFO::read_fifo(const std::vector<std::string>& confs){
    const uint64_t size = confs.size();
    int n_fifos = size;
    std::vector<struct pollfd> pfds;
    std::map<int, std::string> connections;
    unsigned n, k;
    short revents;
    for(n = 0; n < size; n++){
        pfds.push_back(pollfd());
        int fd = open(confs[n].c_str(), O_RDONLY | O_NONBLOCK);
        if (fd == -1){::ers::error(felix2atlas::Felix2atlasIssue(ERS_HERE, std::string("Can't read fifo.")));}
        pfds[n].fd = fd;
        pfds[n].events = POLLIN;
    }
    
    while(n_fifos){
        uint8_t buf[4096] = "";
        int i = poll(&pfds[0], size, -1);
        if (i == -1){::ers::error(felix2atlas::Felix2atlasIssue(ERS_HERE, std::string("Polling error.")));}
        for (n = 0; n < size; n++){
            revents = pfds[n].revents;
            if (revents & POLLIN) {
                k = read(pfds[n].fd, buf, sizeof(buf));
                if(k){
                    // extractField("fid", std::string(buf, buf + sizeof(buf))).empty() ? fid = "0" : fid = extractField("fid", std::string(buf, buf + sizeof(buf)));           
                    on_data(confs[n].c_str(), buf, sizeof(buf), 0);
                    if(!connections.count(pfds[n].fd)){
                        std::string ident = extractField("hostname", std::string(buf, buf + sizeof(buf))) + ".device[" + extractField("device", std::string(buf, buf + sizeof(buf))) +"]";
                        connections[pfds[n].fd] = ident;
                    }
                }
                //printf("POLLIN i=%d k=%d buf=", i, k);
            }
            if (revents & POLLHUP) {
                //close(pfds[n].fd);
                //n_fifos--;
                ::ers::error(felix2atlas::Felix2atlasIssue(ERS_HERE, std::string("Fifo closed: ") + connections[pfds[n].fd]));
                //pfds[n].fd *= -1;            
            }
        }
    }
}

void daq::felix2atlas::SubscribeClientFIFO::connect(Configuration* config)
{   
    const std::vector<daq::felix2atlas::Configuration::ChannelConfig> &channelConfigs = config->getChannelConfig();
    //::ers::debug(felix2atlas::Felix2atlasIssue(ERS_HERE, "running with fifo(s)"));
    std::vector<std::string> confs;
    for (const auto &channelConfig : channelConfigs) {
        std::string fifo = channelConfig.getFifo();
        auto sink = daq::felix2atlas::SinkFactory::createSink(channelConfig.getSinkConfig());
        sinkByFifo.insert({fifo, sink});
        confs.push_back(fifo);
        // for(auto& is : channelConfig.getSinkConfig().mParams){
        //     ::ers::debug(felix2atlas::Felix2atlasIssue(ERS_HERE, std::string("Sink created for fifo: ") + fifo + std::string(" of type: ") + is.first + " : " + is.second));
        // }
    }
    std::thread reader(&daq::felix2atlas::SubscribeClientFIFO::read_fifo, this, confs);
    reader.join();
}


