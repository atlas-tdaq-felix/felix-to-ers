#include "felix2atlas/Configuration.hpp"
#include <fstream>
#include "felix2atlas/FelixERSreceiver.hpp"
#include <ers/ers.h>

std::string daq::felix2atlas::Configuration::ChannelConfig::getFifo() const {
    return mFifo;
}

const daq::felix2atlas::Configuration::ChannelConfig::SinkConfig &daq::felix2atlas::Configuration::ChannelConfig::getSinkConfig() const {
    return sinkConfig;
}

daq::felix2atlas::Configuration::Configuration(struct Config* cfg){
    //mLocalHostname = cfg->localHostName;
    extractConnections(cfg);
}

void daq::felix2atlas::Configuration::extractConnections(struct Config* cfg){

    if (!cfg->isFifo.empty()) {
        if(cfg->partition.empty()){throw felix2atlas::Felix2atlasIssue(ERS_HERE, "No Partition for IS selected.");}
        mConnectionConfigVector.emplace_back(cfg->isFifo, "is");
        mConnectionConfigVector.back().sinkConfig.mParams["partition"] = cfg->partition;
        mConnectionConfigVector.back().sinkConfig.mParams["server"] = cfg->server;
        mConnectionConfigVector.back().sinkConfig.mParams["mode"] = cfg->mode;

        mISInit.emplace_back("traceLevel", "4");
        mISInit.emplace_back("dumpConfiguration", "0");
        mISConfigPresent = true;
    } else {mISConfigPresent = false;}

    if (!cfg->ersFifo.empty()) {
        mConnectionConfigVector.emplace_back(cfg->ersFifo, "ers");
    }

    if(!cfg->ersFifo.empty() && !cfg->isFifo.empty()){::ers::debug(felix2atlas::Felix2atlasIssue(ERS_HERE, "Stats-out and error-out fifo are present. IS and ERS will be forwarded."));} 
    else if (!cfg->isFifo.empty()) {::ers::debug(felix2atlas::Felix2atlasIssue(ERS_HERE, "Stats-out fifo is present. Only IS will be forwarded."));} 
    else if (!cfg->ersFifo.empty()) {::ers::debug(felix2atlas::Felix2atlasIssue(ERS_HERE, "Error-out fifo is present. Only ERS will be forwarded."));}
    else {throw felix2atlas::Felix2atlasIssue(ERS_HERE, "No Fifo selected.");}
}

const std::vector<daq::felix2atlas::Configuration::ChannelConfig> &daq::felix2atlas::Configuration::getChannelConfig() const {
    return mConnectionConfigVector;
}

bool daq::felix2atlas::Configuration::isISConfigPresent() const {
    return mISConfigPresent;
}

const std::list<std::pair<std::string, std::string>> &daq::felix2atlas::Configuration::getISInitList() const {
    return mISInit;
}
