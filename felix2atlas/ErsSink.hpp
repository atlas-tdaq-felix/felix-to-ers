#pragma once
#include "Sink.hpp"
#include "FelixERSreceiver.hpp"

namespace daq::felix2atlas {
    struct ErsSink : public daq::felix2atlas::Sink {
        ErsSink();
        FelixERSreceiver flxers;
        void dispatch(const std::string &message) override;
    };
}

