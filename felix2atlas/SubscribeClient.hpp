#pragma once

#include <fcntl.h> 
#include <poll.h> 
#include <ers/ers.h>
#include <nlohmann/json.hpp>
#include "SinkFactory.hpp"
#include "IsSink.hpp"
#include "Configuration.hpp"
#include "FelixERSreceiver.hpp"

namespace daq::felix2atlas {
    class SubscribeClient {
    public:
        std::map <std::string, daq::felix2atlas::Sink*> sinkByFifo;
        void on_init();
        void on_connect(uint64_t fid);
        void on_disconnect(uint64_t fid);
        void on_data(std::string fifo, const uint8_t* data, size_t size, uint8_t status);
        std::string extractField(std::string field, const std::string &message);
        virtual void connect(Configuration* config) {};
    };

    class SubscribeClientFIFO : public SubscribeClient {

    private:
        void read_fifo(const std::vector<std::string>& confs);

    public:
        void connect(Configuration* config) override;
    };
}