#pragma once

#include <string>
#include "Configuration.hpp"
#include "Sink.hpp"


namespace daq::felix2atlas::SinkFactory {
    daq::felix2atlas::Sink *createSink(const daq::felix2atlas::Configuration::ChannelConfig::SinkConfig &sinkConfig);
}

